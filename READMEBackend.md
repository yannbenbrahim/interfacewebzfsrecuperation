# Le backend

J'utilise NodeJS et Express.  
Pour lancer le serveur, utilisez `npm start`  
Pour lancer les tests, utilisez `npm test`  
Pour savoir la couverture des tests, utilisez `npm run test-with-coverage` :  
    Si c'est la première fois, lancez puis couper avec `CTRL+C` pour voir apparaître le tableau.  
    Si plusieurs fois, supprimez le dossier `.nyc_output`, puis relancer  

## Variable d'environnement

Il faut créer un `.env` dans `./config/`  

### Les variables

`PORT` : Port pour le serveur ( ex : `PORT=1234` )  
`TOKEN_SECRET` : Clé secrete pour la génération de token (ex : `TOKEN_SECRET=amzLkrj3...`)  
`CLIENT_URL` : URL du client (ex : `CLIENT_URL=http://localhost:4321`)  
`SSH_HOST` :  Nom de la machine pour la connexion SSH (ex : `SSH_HOST=iutlan32`)  
`SSH_USER` : Nom de l'utilisateur pour la connexion SSH (ex : `SSH_USER=stephan`)  
`SSH_PRIVATEKEY` : Clé de l'utilisateur pour le SSH (ex : `SSH_PRIVATEKEY=/path/to/id_rsa`)  
`LDAP_URL` : Url du LDAP (ex : `LDAP_URL=ldap://google.fr`)  
`LDAP_BASEDN` : Attribut de base (ex : `LDAP_BASEDN=OU=Moi,DC=Batiment`)  
`LDAP_USERNAME` : Supplément à rajouter pour compléter (ex : `LDAP_USERNAME=@UNIV-RENNES1.COM`)  
`NODE_ENV` : Si en mode production, `NODE_ENV="production"`

## Dépendance

`ActiveDirectory` : Pour se connecter au LDAP de l'IUT  
`ADM-zip` : Pour la création d'un zip  
`body-parser` : Analyse le body d'une requête  
`cookie-parser` : Analyse le cookie d'une requête  
`cors` : Pour configurer le CORS et accepter les requêtes d'une seule source (l'app REACT)  
`dotenv` : Pour acceder aux variables d'environnement  
`express` : Framework  
`helmet` : Protège l'application pour avoir des en-têtes HTTP appropriée  
`jsonwebtoken` : Utilisé pour la création/utilisation d'un cookie chiffré avec une clé de hachage  
`mime` : Pour connaître le type de fichier qu'on va envoyer  
`nodemon` : Pour relancer le serveur dès qu'il y a un changement sur un fichier (Développement)  
`ssh2` : Utilisé pour la connexion en SSH  
`ssh2-promise` : Utilisé pour faire de l'asynchrone en SSH  
