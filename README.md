# Plateforme de récupération de sauvegarde

## Description

Interface web qui utilise le principe des snapshots ZFS pour permettre aux utilisateurs de l'[IUT de Brest-Morlaix](https://www.iut-brest.fr/) d'accéder à d'anciens fichiers ou dossiers présent dans leur home directory.

## Les utilisateurs

Les utilisateurs inscrits dans le LDAP de l'IUT de Brest-Morlaix. 

## Langage de programmation

C'est un projet qui utilise NodeJS, Express et ReactJS.  
L'architecture du projet est grandement inspiré des vidéos faites par [From Scratch : Mern Project](https://www.youtube.com/watch?v=ghdRD3pt8rg).  
J'utilise Create React App et NPM pour une question de simplicité.  

## Installation

Pour installer `node_modules`, il faut aller dans la racine du projet ET dans le dossier `frontend` et utiliser la commande `npm install`  
Normalement, il y aura un problème avec le frontend : Forcer l'installation avec `--force`.  
Ne pas oublier les `.env` (C'est indiqué dans les `README` de chaque dossier). 

Si vous voulez installer sur une nouvelle machine vierge, suivez les étapes suivantes :  
 - Cloner le projet sur la machine  
 - Entrer dans le projet  
 - Installer NodeJS  
 - Installer NPM  
 - Faire `npm install -g n`  
 - Faire `n lst`  
 - Vérifier que nodeJS à la bonne version avec `node -v` (v16.15.1 marche), s'il y a un problème, `hash -r` peut aider. Il y a une section mise à jour.  
 - Faire `npm install npm@latest`  
 - Installer `node_modules` pour le back et le front  
 - Créer et configurer les `.env`  
 - N'oubliez pas les clés SSH  

`apt install nodeJS` ne suffit pas, il faut une version plus récente. La version de NPM dépend de la version de Node, il faut donc d'abord mettre à jour NodeJS.

ATTENTION, dans la version de mise en production (la version actuel), il n'y a pas deux serveur distinct pour le développement. Le principe est donc de lancer le serveur backend en faisant `npm start` dans la racine du projet, et le serveur nodeJS va "piocher" dans le dossier frontend. Il faut donc faire `npm run build` dans le frontend A CHAQUE CHANGEMENT pour créer un dossier `build` qui va permettre la génération du frontend.   

## Problème possible ET connu

Lors de la connexion, la requête de lecture du cookie utilisateur prend du temps (5sec), et fait apparaitre la page d'erreur. La raison du temps pour la requête de lecture est inconnu.

## Mise à jour

Pour mettre à jour NodeJS, la dépendance [n](https://www.npmjs.com/package/n) le fait simplement  
Pour mettre à jour NPM, `npm install npm@latest`  
Pour mettre à jour les dépendences, il faut aller dans chaque dossier (frontend et backend), et utiliser la commande `ncu` de la dépendance `npm-check-updates`.  
Pour installer ncu : `npm i -g npm-check-updates`  
Normalement, il y aura un problème avec le frontend : Forcer l'installation avec `--force`.  

## Licence

[MUI](https://mui.com/core/) : Calendrier, Slider, Switch  
[Chonky File Browser](https://chonky.io/) : Explorateur de fichier  

## Contact

Ben Brahim Yannis  
[yannis.ben29@gmail.com](mailto:yannis.ben29@gmail.com)
