const ActiveDirectory = require('activedirectory')

/* Une fonction qui prend deux paramètres, un nom d'utilisateur et un mot de passe. Il renvoie une
promesse qui se résoudra en un objet utilisateur si l'authentification réussit. */
module.exports.login = (givenUsername, givenPassword) => {
    return new Promise(async (resolve) => {
        const config = {
            url: process.env.LDAP_URL,
            baseDN: process.env.LDAP_BASEDN,
            username: givenUsername + process.env.LDAP_USERNAME,
            password: givenPassword
        };
        const ad = new ActiveDirectory(config);
        ad.findUser(config.username, true, function (err, user) { //Collecte des infos
            if (err) {
                //Erreur
                return resolve(-2);
            }

            if (!user) {
                //Utilisateur inconnu
                return resolve(-3);
            }
            else {  //Réussite de l'opération
                let member = []
                if (typeof(user.memberOf) === 'string') {
                    member.push(user.memberOf.split(",")[0].split("=")[1])
                } else {
                    for(const aMember of user.memberOf) {
                        member.push(aMember.split(",")[0].split("=")[1])
                    }
                }

                user.memberOf = member
                return resolve(user);
            }
        })
    })
}