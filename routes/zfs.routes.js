const router = require('express').Router();
const zfsController = require("../controllers/zfs.controller")

router.post("/chooseSnapshot", zfsController.chooseSnapshot)
router.post("/selectSnapshot", zfsController.selectSnapshot)
router.post("/recover", zfsController.recover)
router.post("/snapshotHomeJSON", zfsController.snapshotHomeJSON);
router.get("/removeSnapshot", zfsController.removeSnapshot)
router.post("/downloadFile", zfsController.downloadFile)
router.post("/getRedirection", zfsController.getRedirection)

module.exports = router;