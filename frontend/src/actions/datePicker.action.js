export const GET_TIME = "GET_TIME"

/**
 * Il prend un temps comme argument et renvoie une fonction qui prend un envoi comme argument et
 * distribue une action avec un type de GET_TIME et une charge utile du temps qui a été passé dans
 * @param time - l'heure que l'utilisateur a sélectionnée
 * @returns Un objet avec un type et une charge utile.
 */
export const getTime = (time) => {
    return (dispatch) => {
        dispatch({ type: GET_TIME, payload: { time: time } })
    }
}