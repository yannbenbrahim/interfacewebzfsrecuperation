export const GET_REDIRECTION = "GET_REDIRECTION"

/**
 * Il prend une redirection comme argument et renvoie une fonction qui prend une fonction de
 * répartition comme argument et distribue une action avec un type de GET_REDIRECTION et une charge utile de
 * l'objet redirection
 * @param redirection - l'objet redirection renvoyé par la base de données
 * @returns Un objet avec un type et une charge utile.
 */
export const getRedirection = (redirection) => {
    return (dispatch) => {
        dispatch({ type: GET_REDIRECTION, payload: { redirection: redirection } })
    }
}