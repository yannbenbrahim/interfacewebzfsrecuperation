export const GET_USER = "GET_USER"

/**
 * Il prend un objet utilisateur comme argument et renvoie une fonction qui prend une fonction de
 * répartition comme argument et distribue une action avec un type de GET_USER et une charge utile de
 * l'objet utilisateur
 * @param user - l'objet utilisateur renvoyé par la base de données
 * @returns Un objet avec un type et une charge utile.
 */
export const getUser = (user) => {
    return (dispatch) => {
        dispatch({ type: GET_USER, payload: { user: user } })
    }
}