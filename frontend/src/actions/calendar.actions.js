export const GET_DATE = "GET_DATE"

/**
 * Il prend une date comme argument, puis distribue une action avec le type GET_DATE et la date comme
 * charge utile
 * @param date - La date que l'utilisateur a sélectionnée.
 * @returns Un objet avec un type et une charge utile.
 */
export const getDate = (date) => {
    return (dispatch) => {
        dispatch({ type: GET_DATE, payload: { date: date } })
    }
}