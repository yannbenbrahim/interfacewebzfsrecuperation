import './styles/index.css';
import Routes from "./components/Routes";
import React, { Component } from 'react';
import { UidContext } from './components/AppContext';
import axios from "axios";
import { connect } from 'react-redux'
import { getUser } from "./actions/user.actions";


class App extends Component {
	constructor(props) {
		super(props)
		this.state = {
			uid: null
		}
	};


	componentDidMount() {
		const fetchToken = async () => {
			await axios({
				method: "get",
				url: `/jwtid`,
				withCredentials: true,
			})
				.then((res) => {
					this.setState({ uid: res.data })
					this.props.getUser(res.data.id);
				})
		};
		fetchToken();
	}

	render() {
		return (
			<UidContext.Provider value={this.state.uid}>
				<Routes />
			</UidContext.Provider>
		);
	}
}

export default connect(null, { getUser })(App);
