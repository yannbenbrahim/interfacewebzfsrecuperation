/**
 * @author Timur Kuzhagaliyev <tim.kuzh@gmail.com>
 * @copyright 2020
 * @license MIT
 */
import axios from 'axios';

import {
    ChonkyActions,
    ChonkyIconName,
    defineFileAction,
    FileBrowser,
    FileContextMenu,
    FileList,
    FileHelper,
    FileNavbar,
    FileToolbar,
} from 'chonky';
import React, { useCallback, useMemo, useState } from 'react';
import FileDownload from "js-file-download";
import b64ToBlob from "b64-to-blob";
import fileSaver from "file-saver";



function FileExplorer(props) {

    const rootFolderId = props.snapshot.rootFolderId;
    var fileMap = props.snapshot.fileMap;


    /**
     * Il retourne les fichiers dans le dossier courant
     * @param currentFolderId - L'identifiant du dossier à partir duquel nous voulons obtenir les
     * fichiers.
     * @returns Un tableau de fichiers
     */
    const useFiles = (currentFolderId) => {
        return useMemo(() => {
            const currentFolder = fileMap[currentFolderId];
            const files = currentFolder.childrenIds
                ? currentFolder.childrenIds.map((fileId) => { var _a; return (_a = fileMap[fileId]) !== null && _a !== void 0 ? _a : null; })
                : [];
            return files;
        }, [currentFolderId]);
    };
    /**
     * Il renvoie un tableau de dossiers qui représentent le chemin du dossier racine au dossier actuel
     * @param currentFolderId - L'identifiant du dossier courant.
     * @returns Un tableau d'objets.
     */
    const useFolderChain = (currentFolderId) => {
        return useMemo(() => {
            const currentFolder = fileMap[currentFolderId];
            const folderChain = [currentFolder];
            let parentId = currentFolder.parentId;
            while (parentId) {
                const parentFile = fileMap[parentId];
                if (parentFile) {
                    folderChain.unshift(parentFile);
                    parentId = parentFile.parentId;
                }
                else {
                    parentId = null;
                }
            }
            return folderChain;
        }, [currentFolderId]);
    };

    /** Détecte si un action a été faite, et agit en conséquence
     */
    const useFileActionHandler = (setCurrentFolderId) => {
        return useCallback((data) => {
            if (data.id === ChonkyActions.OpenFiles.id) {
                const { targetFile, files } = data.payload;
                const fileToOpen = targetFile !== null && targetFile !== void 0 ? targetFile : files[0];
                if (fileToOpen && FileHelper.isDirectory(fileToOpen)) {
                    setCurrentFolderId(fileToOpen.id);
                    return;
                }
            }
            if (data.id === recover.id) {
                if (window.confirm("Vous allez restaurer ce fichier, si le fichier existe encore, il va être écrasé. Êtes-vous sûr ?")) {
                    alert("⚠️ Problème ⚠️ La fonctionnalité est en cours de développement. Utiliser la fonction de Téléchargement en attendant")/*
                    axios({
                        method: "post",
                        url: `/api/zfs/recover`,
                        withCredentials: true,
                        data: {
                            path: data.state.selectedFiles[0].id,
                            isDir: data.state.selectedFiles[0].isDir
                        }
                    })
                        .then(() => alert("La récupération a réussi"))
                        .catch((err) => alert("⚠️ Problème ⚠️", err));*/
                }
            }
            if (data.id === download.id) {
                var type = ""
                if (!data.state.selectedFiles[0].isDir) { type = "blob" }
                axios({
                    method: "post",
                    url: `/api/zfs/downloadFile`,
                    withCredentials: true,
                    data: {
                        fileP: data.state.selectedFiles[0].id,
                        isDir: data.state.selectedFiles[0].isDir
                    },
                    responseType: type
                })
                    .then((res) => {
                        if (data.state.selectedFiles[0].isDir) {
                            const blob = b64ToBlob(res.data, "application/zip");
                            fileSaver.saveAs(blob, `${data.state.selectedFiles[0].name}.zip`);
                        } else {
                            console.log(res);
                            FileDownload(res.data, data.state.selectedFiles[0].name)
                        }

                    })
                    .catch((err) => {
                        console.log(err);
                        alert("⚠️ Problème ⚠️")
                    });
            }
        }, [setCurrentFolderId]);
    };

    const recover = defineFileAction({  // DEFINITION DE MON ACTION CUSTOM
        id: "Recover",
        button: {
            name: 'Restaurer',
            toolbar: true,
            contextMenu: true,
            icon: ChonkyIconName.copy,
        },
        requiresSelection: "true"
    })
    const download = defineFileAction({
        id: "Download",
        button: {
            name: "Télécharger",
            toolbar: true,
            contextMenu: true,
            icon: ChonkyIconName.download,
        },
        requiresSelection: "true"
    })

    const fileActions = [recover, download] //MA LISTE D'ACTION CUSTOM
    const [currentFolderId, setCurrentFolderId] = useState(rootFolderId);
    const files = useFiles(currentFolderId);
    const folderChain = useFolderChain(currentFolderId);
    const handleFileAction = useFileActionHandler(setCurrentFolderId);

    return (
        <div style={{ height: "100%" }}>
            <FileBrowser
                files={files}
                folderChain={folderChain}
                fileActions={fileActions}
                onFileAction={handleFileAction}
            >
                <FileNavbar />
                <FileToolbar />
                <FileList />
                <FileContextMenu />
            </FileBrowser>
        </div>
    );
};

export default FileExplorer;