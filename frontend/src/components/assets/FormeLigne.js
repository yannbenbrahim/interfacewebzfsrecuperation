import React from 'react'

const FormeLigne = () => {
    return ( /*Cas où on est pas connecté*/
        <div className="forme-main">
            <div className="forme-column1">
                <div id="fc11"></div>
                <div id="fc12"></div>
                <div id="fc13"></div>
            </div>
            <div className="forme-column2">
                <div id="fc21"></div>
                <div id="fc22"></div>
                <div id="fc23"></div>
            </div>
            <div className="forme-column3">
                <div id="fc31"></div>
                <div id="fc32"></div>
            </div>
        </div>
    )
}

export default FormeLigne;