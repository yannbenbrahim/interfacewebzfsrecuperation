import React from 'react'
import { Watch } from 'react-loader-spinner'

const Loading = () => {
  return (
    <div className='loading-screen'>
      <Watch
        ariaLabel="loading-indicator"
        color="#0B2839" />
    </div>
  )
}

export default Loading