import React, { useContext } from 'react'
import { NavLink } from 'react-router-dom'
import { UidContext } from "../components/AppContext";
import Logout from './Log/Logout';

const Header = () => {
    const uid = useContext(UidContext);
    return (
        <nav>
            <div className='nav-container'>
                <div className='logo-container'>
                    <NavLink exact="true" to="/">
                        <img src="./img/LogoVectoriel.svg" alt="logo" id='logo' />
                    </NavLink>
                </div>
                {uid && (
                    <div>
                        <Logout />
                    </div>
                )}
            </div>
        </nav>
    )
}

export default Header;