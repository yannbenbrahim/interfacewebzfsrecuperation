import React, { useState } from "react";
import axios from 'axios';

const Login = () => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    /**
     * Il envoie une requête au serveur avec le nom d'utilisateur et le mot de passe entrés par
     * l'utilisateur, et si le serveur répond par une erreur, il l'affiche l'erreur, sinon il redirige
     * l'utilisateur vers la page d'accueil
     * @param e - l'objet événement
     */
    const handleSubmit = (e) => {
        e.preventDefault(); //Evite de recharger la page
        const messageError = document.querySelector('.message-error');
        axios({
            method: 'post',
            url: `/api/user/login`,
            withCredentials: true,
            data: {
                username: username,
                password: password,
            },
        })
            .then((res) => {
                window.location = '/home';
            })
            .catch((err) => //dans le cas où problème
            {
                messageError.innerHTML = err.response.data.errors.message;
            })
    }

    return (
        <div className="connection-form">
            <form action="" onSubmit={handleSubmit} id="login-form">
                <h2 className="message-error"> </h2>
                <label htmlFor="username">Identifiant</label>
                <input type="text" autoComplete='username' name='username' id="username" value={username} onChange={
                    (e) => { setUsername(e.target.value) }
                } />
                <label htmlFor="password">Mot de passe</label>
                <input type="password" autoComplete='current-password' name='password' id="password" value={password} onChange={
                    (e) => setPassword(e.target.value)
                } />
                <input type="submit" value="Se connecter" id="submit" />
            </form>
        </div>
    )
}

export default Login;

