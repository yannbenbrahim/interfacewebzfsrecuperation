import React from "react";
import axios from "axios";
import cookie from "js-cookie";

const Logout = () => {
  /**
   * Il supprime un cookie par clé
   * @param key - Le nom du cookie que vous souhaitez supprimer.
   */
  const removeCookie = (key) => {
    cookie.remove(key, { expires: 1 });
  };

  /**
   * Il envoie une demande au serveur pour déconnecter l'utilisateur, puis il supprime le cookie JWT et
   * le cookie de snapshot, puis il redirige l'utilisateur vers la page de connection
   */
  const logout = async () => {
    await axios({
      method: "get",
      url: `/api/user/logout`,
      withCredentials: true,
    })
      .then(() => removeCookie("jwt", "snapshot"))
      .catch((err) => console.log(err));
    await axios({
      method: "get",
      url: `/api/zfs/removeSnapshot`,
      withCredentials: true,
    })
      .then(() => removeCookie("snapshot"))
      .catch((err) => console.log(err));
    window.location = "/";
  };

  return (
    <button onClick={logout} id="logout">Se déconnecter</button>
  );
};

export default Logout;
