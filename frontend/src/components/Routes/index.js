import React from 'react'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Home from '../../pages/Home'
import Log from '../../pages/Log'
import Save from '../../pages/Save'
import FormeLigne from '../assets/FormeLigne'
import Header from '../Header'
import Helmet from "react-helmet"

const Index = () => {
    return (
        <Router>
            <Helmet>
                <title>Plateforme de récupération</title>
                <meta name="keywords" content="Plateforme, récupération, zfs, IUT, Brest, Morlaix" />
                <meta name="viewport" content="width=device-width"/>
                <meta charset="utf-8" />
                <meta name="Content-Type-Script" content="text/javascript" />
                <meta name="Content-Type-Style" content="text/css" />
                <meta
                    name="description"
                    content="Plateforme de récupération d'anciens fichiers pour l'iut de BREST-MORLAIX"
                />
                <link rel="shortcut icon" href="./img/logoCouleur.png" type="image/x-icon"></link>
            </Helmet>
            <Header />
            <FormeLigne />
            <img src="./img/Carre.svg" alt="logo" id='carre' />
            <Routes>
                <Route path="/" element={<Log />} />
                <Route path="/home" element={<Home />} />
                <Route path="/save" element={<Save />} />
            </Routes>
        </Router>
    )
}

export default Index;