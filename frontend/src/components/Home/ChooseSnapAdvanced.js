import React from "react";
import { useEffect } from "react";
import axios from "axios";

import { useSelector } from "react-redux";

const ChooseSnap = () => {
    const userCalendar = useSelector((state) => state.date.date);
    const userClock = useSelector((state) => state.time.time);
    const redirection = useSelector((state) => state.redirection.redirection)
    const user = useSelector((state) => state.user.user);
    useEffect(() => {
        chooseSnapshot() //Execute a chaque changement de date ou d'heure
    }, [userCalendar, userClock, redirection]);

    /**
     * Il envoie une requête au serveur pour obtenir les snapshots disponibles selon le choix de date
     * et d'heure de l'utilisateur
     */
    const chooseSnapshot = async () => {
        const noSnapshot = document.querySelector('#no-snapshot');
        const snapshotFound = document.querySelector("#snapshotFound")

        if (!userCalendar || !userClock) {
            return noSnapshot.innerHTML = "Pas de sauvegarde trouvé"
        }

        await axios({
            method: 'post',
            url: `/api/zfs/chooseSnapshot`,
            withCredentials: true,
            data: {
                username: user.sAMAccountName,
                redirection : redirection,
                date: userCalendar,
                time: userClock
            },
        })
            .then((res) => {
                if (res.data.message) { //dans le cas où il y a une erreur
                    noSnapshot.innerHTML = res.data.message
                    if (res.data.snapshotNumber >= 1) {
                        snapshotFound.innerHTML = `Il y a ${res.data.snapshotNumber} sauvegardes disponibles pour cette date`
                    }
                    if (res.data.snapshotNumber === 0) {
                        snapshotFound.innerHTML = ''
                    }
                    buttonSnapshot([null])

                } else { //dans le cas où c'est bon
                    snapshotFound.innerHTML = `Il y a ${res.data.snapshotNumber} sauvegardes disponibles pour cette date`
                    noSnapshot.innerHTML = ""
                    buttonSnapshot(res.data.snapshots)

                }
            })
            .catch((err) => //dans le cas où problème
            {
                console.log("Erreur lors de la connexion :", err)
            })
    }

    /**
     * Il envoie une demande au serveur pour sélectionner une snapshot, puis redirige l'utilisateur
     * vers la page Save
     * @param e - l'événement qui a déclenché la fonction
     */
    function selectSnapshot(e) {
        //Info sur quel bouton on a appuyé e.target.id
        axios({
            method: 'post',
            url: `/api/zfs/selectSnapshot`,
            withCredentials: true,
            data: {
                username: user.sAMAccountName,
                group: user.memberOf,
                snapshot: e.target.id, //IL faut le nom en brut
                redirection: redirection
            },
        })
            .then((res) => {
                if (res.data.errors) { //dans le cas où il y a une erreur
                    console.log("Pas de sauvegarde trouvé");
                } else { //dans le cas où c'est bon
                    window.location = "/save"
                }
            })
            .catch((err) => //dans le cas où problème
            {
                console.log("Erreur lors de la connexion :", err)
            })
    }


    //Pour afficher le bon nombre de bouton selon le nombre de snapshot disponible
    var buttonSnapshot = (snapshots) => {
        var lesButtons = document.querySelector(".buttons")
        if (snapshots[0] != null) { //Si il y a quelque chose dans le tableau
            lesButtons.innerHTML = "" //Reset le dedans
            snapshots.forEach(element => {
                var button = document.createElement("button")
                button.className = "buttonSnap"
                button.id = element
                button.addEventListener("click", selectSnapshot);
                var text = element.split("-")[6]
                text = text.charAt(0) + text.charAt(1) + "h" + text.charAt(2) + text.charAt(3)
                button.innerHTML = text
                lesButtons.appendChild(button)
            });
        } else { //Si y'a rien
            lesButtons.innerHTML = ""
        }
    }

    return (
        <div>
            <h3 id="snapshotFound"> </h3>
            <div className="selection">
                <h1 id="no-snapshot"> </h1>
                <div className="buttons">
                </div>
            </div>
        </div>
    );
};

export default ChooseSnap;
