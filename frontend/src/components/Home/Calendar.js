/*FROM MUI X*/

import * as React from 'react';

import { useEffect } from "react";
import frLocale from 'date-fns/locale/fr';
import TextField from '@mui/material/TextField';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';

import { useDispatch } from "react-redux";
import { getDate } from "../../actions/calendar.actions";

export default function LocalizedDatePicker(props) {
  const [value, setValue] = React.useState(new Date());
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getDate(JSON.stringify(value))) //Il faut une valeur serialisable
  }, [value, dispatch]);

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns} locale={frLocale}>
      <div>
        <DatePicker
          mask={'__/__/____'}
          value={value}
          onChange={(newValue) => setValue(newValue)}
          renderInput={(params) => <TextField {...params} />}
        />
      </div>
    </LocalizationProvider>
  );
}