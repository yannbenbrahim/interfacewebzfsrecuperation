import * as React from 'react';
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';

import { useDispatch } from "react-redux";
import { getTime } from "../../actions/datePicker.action";

export default function RangeSlider() {
    const [value, setValue] = React.useState([2, 6]);
    const dispatch = useDispatch();
    React.useEffect(() => {
        dispatch(getTime(value)) //Il faut une valeur serialisable
    }, [value, dispatch]);

    function valuetext(value) {
        return `${value}h`;
    }

    const marks = [
        {
            value: 0,
            label: "0h"
        },
        {
            value: 2,
            label: "2h"
        },
        {
            value: 4,
            label: "4h"
        },
        {
            value: 6,
            label: "6h"
        },
        {
            value: 8,
            label: "8h"
        },
        {
            value: 10,
            label: "10h"
        },
        {
            value: 12,
            label: "12h"
        },
        {
            value: 14,
            label: "14h"
        },
        {
            value: 16,
            label: "16h"
        },
        {
            value: 18,
            label: "18h"
        },
        {
            value: 20,
            label: "20h"
        },
        {
            value: 22,
            label: "22h"
        },
    ]
    const minDistance = 1;

    const handleChange = (event, newValue, activeThumb) => {
        if (!Array.isArray(newValue)) {
            return;
        }

        if (activeThumb === 0) {
            setValue([Math.min(newValue[0], value[1] - minDistance), value[1]]);
        } else {
            setValue([value[0], Math.max(newValue[1], value[0] + minDistance)]);
        }
    };


    return (
        <Box sx={{ width: 330 }}>
            <Slider
                getAriaLabel={() => 'Hour range'}
                value={value}
                step={1}
                min={0}
                max={23}
                marks={marks}
                onChange={handleChange}
                valueLabelFormat={valuetext}
                valueLabelDisplay="on"
                getAriaValueText={valuetext}
                disableSwap
            />
        </Box>
    );
}