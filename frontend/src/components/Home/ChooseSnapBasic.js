import React from 'react'
import { useEffect } from "react";
import axios from "axios";

import { useSelector } from "react-redux";

const ChooseSnapBasic = () => {
	const user = useSelector((state) => state.user.user);
	const redirection = useSelector((state) => state.redirection.redirection)

    useEffect(() => {
        chooseSnapshot() //Execute a chaque changement de date ou d'heure
    }, [redirection]);

	/**
	 * Il envoie une requête au serveur pour obtenir les snapshots du répertoire personnel de
	 * l'utilisateur, puis il affiche les snapshots dans un menu de buttons
	 */
	const chooseSnapshot = async () => {
		if(!redirection) return null
		await axios({
			method: 'post',
			url: `/api/zfs/chooseSnapshot`,
			withCredentials: true,
			data: {
				redirection : redirection,
				username: user.sAMAccountName,
			},
		})
			.then((res) => {
				buttonSnapshot(res.data.snapshots)
			})
			.catch((err) => //dans le cas où problème
			{
				console.log("Erreur lors de la connexion :", err)
			})
	}
	function selectSnapshot(e) { //!!!! PRENDS QUE LE PREMIER BOUTON
		//Info sur quel bouton on a appuyé e.target.id
		axios({
			method: 'post',
			url: `/api/zfs/selectSnapshot`,
			withCredentials: true,
			data: {
				username: user.sAMAccountName,
				group: user.memberOf,
				snapshot: e.target.id, //IL faut le nom en brut
				redirection: redirection
			},
		})
			.then((res) => {
				if (res.data.errors) { //dans le cas où il y a une erreur
					console.log("Pas de sauvegarde trouvé");
				} else { //dans le cas où c'est bon
					window.location = "/save"
				}
			})
			.catch((err) => //dans le cas où problème
			{
				console.log("Erreur lors de la connexion :", err)
			})
	}

	const buttonSnapshot = (snapshots) => {
		var lesButtons = document.querySelector(".buttons-basique")
		if (snapshots[0] != null) { //Si il y a quelque chose dans le tableau
			lesButtons.innerHTML = "" //Reset le dedans
			var button1 = document.createElement("button")
			button1.className = "buttonSnap"
			button1.id = snapshots[0]
			button1.addEventListener("click", selectSnapshot);
			button1.innerHTML = "Sauvegarde la plus récente"
			lesButtons.appendChild(button1)
			var button2 = document.createElement("button")
			button2.className = "buttonSnap"
			button2.id = snapshots[1]
			button2.addEventListener("click", selectSnapshot);
			button2.innerHTML = "Sauvegarde de la journée"
			lesButtons.appendChild(button2)
			var button3 = document.createElement("button")
			button3.className = "buttonSnap"
			button3.id = snapshots[2]
			button3.addEventListener("click", selectSnapshot);
			button3.innerHTML = "Sauvegarde de la semaine"
			lesButtons.appendChild(button3)
			var button4 = document.createElement("button")
			button4.className = "buttonSnap"
			button4.id = snapshots[3]
			button4.addEventListener("click", selectSnapshot);
			button4.innerHTML = "Sauvegarde du mois"
			lesButtons.appendChild(button4)
		} else { //Si y'a rien
			lesButtons.innerHTML = ""
		}
	}
	return (
		<div>
			<div className="buttons-basique">
			</div>
		</div>
	)
}

export default ChooseSnapBasic
