import * as React from 'react';
import Box from '@mui/material/Box';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

import axios from 'axios';
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { getRedirection } from "../../actions/redirection.action";

export default function CenteredTabs() {
    const user = useSelector((state) => state.user.user);
    const [value, setValue] = React.useState("homes");
    const [tabId, setTabId] = React.useState(0);
    const [redirection, setRedirection] = React.useState([]);
    const dispatch = useDispatch();
    React.useEffect(() => {
        if (value === "homes") { // cas home
            dispatch(getRedirection("homes"))
        } else if (value === "redirection") { // cas windows
            dispatch(getRedirection("windows2"))
        } else {
            dispatch(getRedirection(value))
        }

    }, [value, dispatch]);
    React.useEffect(() => {
        getUserRedirection()
    }, [user]);

    const handleTabChange = (event, newTabId) => {
        setTabId(newTabId);
        setValue(newTabId);
    };

    //Last item of a array
    const lastItem = (array) => {
        return array[array.length - 1];
    }

    const getUserRedirection = async () => {
        await axios({
            method: 'post',
            url: `/api/zfs/getRedirection`,
            withCredentials: true,
            data: {
                memberOf: user.memberOf
            },
        })
            .then((res) => {
                let tabs = [];
                for (const aGroupRedirection in res.data.redirectionListAvailable) {
                    for (const aRedirection in res.data.redirectionListAvailable[aGroupRedirection]) {
                        tabs.push(<Tab value={res.data.redirectionListAvailable[aGroupRedirection][aRedirection]} label={lastItem(res.data.redirectionListAvailable[aGroupRedirection][aRedirection].split("/"))} />)
                    }
                }
                setRedirection(tabs);
            })
            .catch((err) => {//dans le cas où problème
                console.log("Erreur", err);
            })
    }
    return (
        <Box sx={{ width: '500' }}>
            <Tabs
                value={tabId}
                onChange={handleTabChange}
                variant="scrollable"
                scrollButtons="on"
                centered
            >
                <Tab label="Home" value="homes" key={0} />
                <Tab label="Redirection" value="redirection" />
                {redirection.map(child => child)}
            </Tabs>
        </Box>
    )
}
/*
    const tabs = [<Tab label={`New Tab `} />]


    return (
        <Box sx={{ width: '500' }}>
            <Tabs
                value={tabId}
                onChange={handleTabChange}
                variant="scrollable"
                scrollButtons="on"
                centered
            >
                <Tab label="Home" value="homes" key={0} />
                <Tab label="Redirection" value="redirection" />
                {tabs.map(child => child)}
            </Tabs>
        </Box>
    );


import * as React from 'react';
import Box from '@mui/material/Box';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { getRedirection } from "../../actions/redirection.action";
import { render } from 'react-dom';

export default function CenteredTabs() {
    const [value, setValue] = React.useState("homes");
    const user = useSelector((state) => state.user.user);
    const dispatch = useDispatch();

    const [tabId, setTabId] = React.useState(0);
    const handleTabChange = (event, newTabId) => {
        if (value === "homes") { // cas home
            dispatch(getRedirection("homes"))
        } else if (value === "redirections") { // cas redirection windows
            dispatch(getRedirection("windows2"))
        }
        setTabId(newTabId);
    };


    //Ajout des tabs pour les différents types de redirection

    const [tabs, setTabs] = React.useState([])
    setTabs([
        ...tabs,
        <Tab label={`New Tab `} />
    ]);

    return (
        <div>
            <Tabs
                value={tabId}
                onChange={handleTabChange}
                variant="scrollable"
                scrollButtons="on"
            >
                <Tab label="Homes" value="home" />
                <Tab label="Redirection" value="redirection" />
                {tabs.map(child => child)}
            </Tabs>
        </div>
    );
}*/