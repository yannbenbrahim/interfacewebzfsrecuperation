import { GET_DATE } from "../actions/calendar.actions";

const initialState = {};

export default function calendarReducer(state = initialState, action) {
    switch (action.type) {
        case GET_DATE:
            return action.payload;
        default:
            return state;
    }
}