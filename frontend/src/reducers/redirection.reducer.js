import { GET_REDIRECTION } from "../actions/redirection.action"

const initialState = {};

export default function redirectionReducer(state = initialState, action) {
    switch (action.type) {
        case GET_REDIRECTION:
            return action.payload;
        default:
            return state;
    }
}