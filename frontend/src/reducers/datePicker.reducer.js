import { GET_TIME } from "../actions/datePicker.action";

const initialState = {};

export default function datePickerReducer(state = initialState, action) {
    switch (action.type) {
        case GET_TIME:
            return action.payload;
        default:
            return state;
    }
}