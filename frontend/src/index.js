import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './styles/index.css'
import { setChonkyDefaults } from 'chonky';
import { ChonkyIconFA } from 'chonky-icon-fontawesome';

//Redux
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import userReducer from "./reducers/user.reducer";
import datePickerReducer from './reducers/datePicker.reducer';
import calendarReducer from './reducers/calendar.reducer';
import redirectionReducer from './reducers/redirection.reducer'

setChonkyDefaults({ iconComponent: ChonkyIconFA });


const root = ReactDOM.createRoot(document.getElementById('root'));

const store = configureStore({
  reducer: {
    user: userReducer,
    time: datePickerReducer,
    date: calendarReducer,
    redirection : redirectionReducer
  }
})

root.render(
  <Provider store={store}>
    <App />
  </Provider>
);
