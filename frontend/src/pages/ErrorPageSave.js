import React, { Component } from 'react'

const NoJSON = (props) => {
    const handleSubmit = (event) => {
        event.preventDefault()
        window.location = "/"
    }
    console.log(props);
    return (
        <div className="error-main">
            <h2>{props.error}</h2>
            <h2>Vous pouvez contacter le service informatique en cas de problème</h2>
            <button onClick={handleSubmit}>Retour à la page de connexion</button>
        </div>
    )
}
export default NoJSON;