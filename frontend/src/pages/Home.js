import React, { useContext, useEffect, useState } from 'react'
import { UidContext } from "../components/AppContext";
import Switch from '@mui/material/Switch';
import NotLog from "./ErrorPageLog";
import Calendar from '../components/Home/Calendar'
import DatePicker from '../components/Home/Slider';
import ChooseSnapAdvanced from "../components/Home/ChooseSnapAdvanced";
import ChooseSnapBasic from '../components/Home/ChooseSnapBasic';
import Loading from '../components/Loading';
import Redirection from "../components/Home/Redirection"


const Home = () => {
	const uid = useContext(UidContext);
	const [checked, setChecked] = useState(false);
	const [isLoading, setIsLoading] = useState(true);

	useEffect(() => {
		setTimeout(() => {
			setIsLoading(false);
		}, 500);
	});

	const handleChange = (event) => {
		setChecked(event.target.checked);
	};

	if (isLoading) {
		return (
			<Loading />
		)
	} else {
		if (uid) {
			return ( /*Cas où on est connecté*/
				<div className="home-main">
					<Redirection/>
					<h1>Plateforme de récupération</h1>
					<div className="switch">
						<Switch
							checked={checked}
							onChange={handleChange}
							inputProps={{ 'aria-label': 'controlled' }}
						/>
						<h3>Recherche avancée</h3>
					</div>

					{checked ? (
						<div className="advance-mode">
							<h2>Sélectionner une date et une plage horaire</h2>
							<div className='date-picker'>
								<div className='calendar'>
									<Calendar />
								</div>
								<div className='clock'>
									<DatePicker />
								</div>
							</div>
							<ChooseSnapAdvanced />
						</div>
					) : (
						<div className='basic-mode'>
							<h2>Sélectionner une sauvegarde</h2>
							<ChooseSnapBasic />
						</div>
					)}

				</div>
			)
		} else {
			return ( /*Cas où on est pas connecté*/
				<div className="notlog-main">
					<NotLog />
				</div>
			)
		}
	}

}

export default Home;