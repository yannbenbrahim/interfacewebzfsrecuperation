import React, { Component } from 'react'

const NotLogged = () => {
    const handleSubmit = (event) => {
        event.preventDefault()
        window.location = "/"
    }
        return (
            <div className="error-main">
                <h1>Erreur 400</h1>
                <h2>La page demandé n'est pas accessible car vous n'êtes pas connecté</h2>
                <button onClick={handleSubmit}>Retour à la page de connexion</button>
            </div>
        )
}
export default NotLogged;