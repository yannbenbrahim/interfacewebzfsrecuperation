import React, { useContext } from 'react'
import Login from "../components/Log";
import { UidContext } from "../components/AppContext";

const Log = () => {
	const uid = useContext(UidContext);

	return (
		<div className="log-main">
			{
				uid ?
					window.location = "/home"
					: (
						<div>
							<div className="log-title">
								<h2>Plateforme de récupération</h2>
								<p>Permet de récupérer d'anciennes versions de fichiers en cas de perte ou d'endommagement </p>
							</div>
							<div>
								<Login />
							</div>
						</div>
					)}

		</div>
	)
}

export default Log;