import React, { useContext, useEffect, useState } from 'react'
import { UidContext } from "../components/AppContext";
import SystemFile from '../components/FileExplorer/FileExplorer';
import Loading from '../components/Loading';
import NoJSON from "./ErrorPageSave";
import axios from 'axios';
import cookie from "js-cookie";


const Save = () => {
	const uid = useContext(UidContext);
	const [snapshotId, setSnapshotId] = useState("");
	const [snapshotJSON, setSnapshotJSON] = useState(false);
	const [isLoading, setIsLoading] = useState(true);
	const [error, setError] = useState("")



	/**
	 * Il supprime un cookie par clé
	 * @param key - Le nom du cookie que vous souhaitez supprimer.
	 */
	const removeCookie = (key) => {
		cookie.remove(key, { expires: 1 });
	};

	/**
	 * Il supprime le cookie de snapshot et redirige l'utilisateur vers la page d'accueil
	 */
	const removeSnapshot = async () => {
		await axios({
			method: "get",
			url: `/api/zfs/removeSnapshot`,
			withCredentials: true,
		})
			.then(() => removeCookie("snapshot"))
			.catch((err) => console.log(err));

		window.location = "/home";
	};

	useEffect(() => {
		/**
		 * Il récupère l'identifiant de snapshot, puis l'utilise pour récupérer
		 * un fichier JSON représentant la snapshot
		 */
		const fetchToken = async () => {

			await axios({  //Cookie de la snapshot
				method: "get",
				url: `/snapshot`,
				withCredentials: true,
			})
				.then(async (res) => {
					setSnapshotId(res.data.id.snapshot)
					await axios({
						method: "post",
						url: `/api/zfs/snapshotHomeJSON`,
						withCredentials: true,
						data: {
							redirection : res.data.id.redirection,
							user: res.data.id.username,
							snapshot: res.data.id.snapshot,
							group: res.data.id.group
						}
					})
						.then((res) => {
							setSnapshotJSON(res.data.root)
							setIsLoading(false);
						})
						.catch((err) => {
							setError(err.response.data)
							setSnapshotJSON(null)
							setIsLoading(false)
						});
				})
				.catch((err) => console.log("No token from frontend", err));
		}
		fetchToken()

	}, []);
	// Titre dynamique de la snapshot
	const titleFormat = () => {
		var time = snapshotId.split("-")
		var heure = time[6].charAt(0) + time[6].charAt(1) + "h" + time[6].charAt(2) + time[6].charAt(3)
		return `Sauvegarde du ${time[5]}/${time[4]} à ${heure}`
	}

	if (isLoading) {
		return (
			<Loading />
		)
	} else {
		if (uid && snapshotJSON) {
			return ( /*Cas où on est connecté*/
				<div className="save-main">

					<button id="retour" onClick={removeSnapshot}>&lt;Retour à l'accueil</button>
					<h1>{titleFormat()}</h1>
					<div className='system-file-window'>
						<SystemFile snapshot={snapshotJSON} />
					</div>
				</div>
			)
		} else if (!snapshotJSON || !uid) {
			return ( /*Cas où on est pas connecté*/
				<div className="notlog-main">
					<NoJSON error={error}/>
				</div>
			)
		}
	}
}


export default Save;