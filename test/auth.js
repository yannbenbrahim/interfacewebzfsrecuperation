const chai = require('chai');
const chaiHttp = require('chai-http');
const { response } = require('express');
const server = require('../server');

chai.use(chaiHttp);
chai.should();

describe('Authentification', () => {
    describe('Fonction login', () => {
        it("Bon login", (done) => {
            const log = {
                username: "gea1a",
                password: "si-A015*"
            }
            chai.request(server)
                .post('/api/user/login')
                .send(log)
                .end((err, response) => {
                    response.should.have.status(202)
                    response.body.should.be.a("object")
                    done()
                })
        })
        it("Mauvais login, bon mot de passe", (done) => {
            const log = {
                username: "gea1ab",
                password: "si-A015*"
            }
            chai.request(server)
                .post('/api/user/login')
                .send(log)
                .end((err, response) => {
                    response.should.have.status(400)
                    done()
                })
        })
        it("Bon login, mauvais mot de passe", (done) => {
            const log = {
                username: "gea1ab",
                password: "si-A015"
            }
            chai.request(server)
                .post('/api/user/login')
                .send(log)
                .end((err, response) => {
                    response.should.have.status(400)
                    done()
                })
        })
    })
});
