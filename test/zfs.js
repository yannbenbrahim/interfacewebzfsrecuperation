const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');

chai.use(chaiHttp);
chai.should();

describe('ZFS', async () => {
    describe('Fonction chooseSnapshot', async () => {
        it('Sans rien', (done) => {
            const req = {}
            chai.request(server)
                .post("/api/zfs/chooseSnapshot")
                .send(req)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.be.a("object")
                    res.body.should.have.property("error").eq("Nothing in body")
                    done()
                })
        });
        it('Avec username : Recherche basique', (done) => {
            const req = {
                username: "ybenbrahim"
            }
            chai.request(server)
                .post("/api/zfs/chooseSnapshot")
                .send(req)
                .end((err, res) => {
                    res.should.have.status(202)
                    res.body.should.be.a("object")
                    res.body.should.have.property("snapshots").lengthOf(4)
                    done()
                })
        });
        it('Avec username, date et heure (Snap non dispo à la date donnée)', (done) => {
            const req = {
                username: "ybenbrahim",
                date: "2020-03-03T11:06:15.396Z",
                time: [8, 10]
            }
            chai.request(server)
                .post("/api/zfs/chooseSnapshot")
                .send(req)
                .end((err, res) => {
                    res.should.have.status(202)
                    res.body.should.be.a("object")
                    res.body.should.have.property("snapshotNumber").eq(0)
                    done()
                })
        });
        it("Avec username, date et heure (Snap non dispo à l'heure donnée)", (done) => {
            const req = { //Changer l'heure pour trouver une snapshot existante mais pas à la bonne heure
                username: "ybenbrahim",
                date: "2022-01-22T11:06:15.396Z",
                time: [6, 8]
            }
            chai.request(server)
                .post("/api/zfs/chooseSnapshot")
                .send(req)
                .end((err, res) => {
                    res.should.have.status(202)
                    res.body.should.be.a("object")
                    res.body.should.have.property("message").eq('Pas de sauvegarde de 6h à 8h')
                    res.body.should.have.property("snapshotNumber")
                    done()
                })

        });
        it('Avec username, date et heure (Snap non dispo car heure trop grande)', (done) => {
            const req = {
                username: "ybenbrahim",
                date: "2022-05-12T11:06:15.396Z",
                time: [6, 22]
            }
            chai.request(server)
                .post("/api/zfs/chooseSnapshot")
                .send(req)
                .end((err, res) => {
                    res.should.have.status(202)
                    res.body.should.be.a("object")
                    res.body.should.have.property("message").eq(`Plage horaire trop grande, veuillez réduire la taille`)
                    res.body.should.have.property("snapshotNumber")
                    done()
                })
        });
    })
    //Comme il n'y a pas d'accès aux cookies, il y aura forcément un problème.
    //Pour Test, il faut enlever la variable isAdmin, et tout ce qui est rattaché avec
    /*
    describe('Fonction selectSnapshot', async () => { 
        it('Une snapshot connu', (done) => {
            const req = {
                snapshot: "zfs-auto-snap_hourly-2022-04-23-0317",
                username: "ybenbrahim"
            }
            chai.request(server)
                .post("/api/zfs/selectSnapshot")
                .send(req)
                .end((err, res) => {
                    res.should.have.status(202)
                    res.body.should.be.a("object")
                    res.body.should.have.property("snapshot").eq(req.snapshot)
                    res.body.should.have.property("username").eq(req.username)
                    done()
                })
        });
        it('Une snapshot inconnu', (done) => {
            const req = {
                snapshot: "zfs-auto-snap_inconnu-2022-04-23-0317",
                username: "ybenbrahim"
            }
            chai.request(server)
                .post("/api/zfs/selectSnapshot")
                .send(req)
                .end((err, res) => {
                    res.should.have.status(404)
                    res.body.should.be.a("object")
                    res.body.should.have.property("message").eq("Snapshot inconnue !")
                    done()
                })
        });

    })
    */
    describe('Fonction snapshotHomeJSON', async () => {
        it('Une snapshot avec un JSON qui marche', (done) => { //Changer l'heure pour trouver une snapshot existante
            const req = {
                snapHomeDirectory: "/tank/homes/.zfs/snapshot/zfs-auto-snap_frequent-2022-05-30-0500/gea1a"
            }
            chai.request(server)
                .post("/api/zfs/snapshotHomeJSON")
                .send(req)
                .end((err, res) => {
                    res.should.have.status(202)
                    res.body.should.be.a("object")
                    res.body.should.have.property("root").property("rootFolderId").eq("/tank/homes/.zfs/snapshot/zfs-auto-snap_frequent-2022-05-30-0500/gea1a")
                    res.body.should.have.property("root").property("fileMap")
                    done()
                })
        }).timeout(5000);
    })
});
