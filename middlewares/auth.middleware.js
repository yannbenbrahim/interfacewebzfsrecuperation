const jwt = require('jsonwebtoken');

/** Vérifie si le token représentant l'utilisateur est valide
 * @requires cookie Le cookie "jwt"
 * @res Si le token est valide, renvoie les infos du token. Sinon, erreur
*/
module.exports.requireAuth = (req, res, next) => {
    const token = req.cookies.jwt;
    if (token) {
        try {
            jwt.verify(token, process.env.TOKEN_SECRET, async (err, decodedToken) => {
                if (err) {
                    console.log("erreur de token", err);
                    res.cookie('jwt', '', { maxAge: 1 })
                    res.status(400).json('Token removed')
                } else {
                    res.locals.user = decodedToken
                    next();
                }
            });
        } catch (error) {
            console.log("erreur de token", err);
            res.cookie('jwt', '', { maxAge: 1 })
            res.status(400).json('Token removed')
        }
    }
};