const jwt = require('jsonwebtoken');

/** Vérifie si le token représentant une snapshot sélectionné est valide
 * @requires cookie Le cookie "snapshot"
 * @res Si le token est valide, renvoie les infos du token. Sinon, erreur
*/
module.exports.requireSnap = (req, res, next) => {
    const token = req.cookies.snapshot;
    if (token) {
        try {
            jwt.verify(token, process.env.TOKEN_SECRET, async (err, decodedToken) => {
                if (err) {
                    console.log("erreur de token", err);
                    res.cookie('snapshot', '', { maxAge: 1 })
                    res.sendStatus(400).json('Token removed')
                } else {
                    res.locals.user = decodedToken
                    next();
                }
            });
        } catch (error) {
            console.log("erreur de token", err);
            res.cookie('jwt', '', { maxAge: 1 })
            res.send(400).json('Token removed')
        }

    } else {
        console.log('No token from backend');
    }
};