/* Une fonction qui renvoie un message d'erreur si l'utilisateur n'est pas trouvé dans l'annuaire LDAP. */
module.exports.signInErrors = (err) => {
    let errors = { message: "" }

    errors.message = "L'utilisateur et/ou le mot de passe est incorrect";

    return errors;
}