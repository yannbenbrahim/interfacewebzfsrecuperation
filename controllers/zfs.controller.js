const jwt = require("jsonwebtoken");
const fs = require('fs');
const pth = require("path");
const AdmZip = require('adm-zip');
const { Client } = require('ssh2');
const mime = require("mime");
const SSH2Promise = require('ssh2-promise');



/**
 * Renvoie le dernier élément d'un tableau ou les n derniers éléments d'un tableau.
 * @param array - un tableau de n'importe quel type
 * @param int - le nombre d'articles à retourner
 * @returns Le dernier élément du tableau.
 */
function last(array, int) {
    int++
    return array[array.length - int];
}

/** Une fonction qui est appelée lorsque l'utilisateur veut choisir un instantané.
 * Utilise une connexion SSH où les paramères sont des variables d'environnement
 * Si il n'y a que 1 paramètre, c'est que c'est la recherche basique
 * @requires username - Pour savoir si dans une snapshot, il y a l'utilisateur
 * @requires date - Pour avoir une snapshot selon une date choisie par l'utilisateur
 * @requires time - Pour avoir une snapshot selon une plage horaire choisie par l'utilisateur
 * @returns un tableau de snapshot, le nombre de snapshot trouvé et un message s'il y a un problème. Si recherche basique, ne retourne que un tableau de 4 snapshots
*/
module.exports.chooseSnapshot = (req, res) => {
    const { date, time, username, redirection } = req.body;  //!!!!!
    if (!username || !redirection) return res.status(400).json({ error: "Nothing in body" })
    let pathSnapshot = null
    if (redirection === "homes" || redirection === "windows2") {
        pathSnapshot = process.env.PATH_SNAPSHOT + redirection + "/.zfs/snapshot/" //!!!!!    
    } else {
        let redirectionFormat= redirection.split("/")
        pathSnapshot = process.env.PATH_SNAPSHOT + redirectionFormat[0] + "/" + redirectionFormat[1] + "/.zfs/snapshot/"
    }
    const conn = new Client();
    conn.on('ready', () => {
        conn.sftp((err, sftp) => {
            if (err) throw err;
            sftp.readdir(pathSnapshot, (err, snapshots) => {
                if (err) throw err;
                if (date && time) { //Recherche avancée
                    dateCorrigé = date.split('"').join('')
                    //Date recherché dans format AAAA-MM-JJ
                    dateFormat = dateCorrigé.split("T")
                    dateFormat = dateFormat[0].split("-")
                    dateFormat = dateFormat[0] + "-" + dateFormat[1] + "-" + dateFormat[2]
                    snapshotsFormat = []

                    snapshots.forEach(aSnapshot => {
                        if (aSnapshot["filename"].split("_").length == 2) snapshotsFormat.push(aSnapshot["filename"])
                    });

                    snapshotsMatch = snapshotsFormat.filter(function (value) {
                        snapshotFormat = value.split("-")
                        snapshotFormat = last(snapshotFormat, 3) + "-" + last(snapshotFormat, 2) + "-" + last(snapshotFormat, 1)
                        return snapshotFormat === dateFormat
                    });
                    snapshotsMatchHour = snapshotsMatch.filter(function (value) {
                        return parseInt(last(value.split("-"), 0).match(/.{1,2}/g)[0]) >= time[0] && parseInt(last(value.split("-"), 0).match(/.{1,2}/g)[0]) <= time[1]
                    });
                    if ((time[1] - time[0]) > 8) { //Trop de snapshot a afficher
                        return res.status(202).json({ message: `Plage horaire trop grande, veuillez réduire la taille`, snapshotNumber: snapshotsMatch.length })
                    }
                    else if (snapshotsMatchHour.length === 0 && snapshotsMatch.length >= 1) { //Pas de snapshot trouvé pour cette heure, mais snapshot pour ce jour
                        return res.status(202).json({ message: `Pas de sauvegarde de ${time[0]}h à ${time[1]}h`, snapshotNumber: snapshotsMatch.length })
                    } else if (snapshotsMatchHour.length >= 1) { // Snapshot trouvé pour le jour et l'heure donnée
                        return res.status(202).json({ snapshots: snapshotsMatchHour, snapshotNumber: snapshotsMatch.length })
                    } else if (snapshotsMatch.length === 0) {
                        return res.status(202).json({ message: `Pas de sauvegarde au jour : ${dateFormat}`, snapshotNumber: snapshotsMatch.length })
                    }

                } else { //Recherche basique

                    snapshotsFormat = []
                    snapshots.forEach(aSnapshot => {
                        if (aSnapshot["filename"].split("_").length == 2) snapshotsFormat.push(aSnapshot["filename"])
                    });

                    const nowDate = new Date()
                    const nowDateFormat = `X_X-${nowDate.getFullYear()}-${timeFormat(nowDate.getMonth() + 1)}-${timeFormat(nowDate.getDate())}-${timeFormat(nowDate.getHours())}${timeFormat(nowDate.getMinutes())}`
                    var morningDate = nowDate;
                    morningDate.setHours(3)
                    morningDate = `X_X-${morningDate.getFullYear()}-${timeFormat(morningDate.getMonth() + 1)}-${timeFormat(morningDate.getDate())}-${timeFormat(morningDate.getHours())}${timeFormat(morningDate.getMinutes())}`
                    var weekDate = nowDate;
                    weekDate.setDate(nowDate.getDate() - 7)
                    weekDate = `X_X-${weekDate.getFullYear()}-${timeFormat(weekDate.getMonth() + 1)}-${timeFormat(weekDate.getDate())}-${timeFormat(weekDate.getHours())}${timeFormat(weekDate.getMinutes())}`
                    var monthDate = nowDate;
                    monthDate.setMonth(nowDate.getMonth() - 1)
                    monthDate = `X_X-${monthDate.getFullYear()}-${timeFormat(monthDate.getMonth() + 1)}-${timeFormat(monthDate.getDate())}-${timeFormat(monthDate.getHours())}${timeFormat(monthDate.getMinutes())}`
                    var selectedSnapshots = ["X_X-1970-01-01-0000", "X_X-1970-01-01-0000", "X_X-1970-01-01-0000", "X_X-1970-01-01-0000"] // 0: newest | 1: today morning | 2: week | 3: month
                    for (const aSnapshot of snapshotsFormat) {
                        if (!snapshotComparaison(aSnapshot, nowDateFormat) && snapshotComparaison(aSnapshot, selectedSnapshots[0])) { //Newest
                            selectedSnapshots[0] = aSnapshot
                        }
                        if (!snapshotComparaison(aSnapshot, morningDate) && snapshotComparaison(aSnapshot, selectedSnapshots[1])) { //today morning
                            selectedSnapshots[1] = aSnapshot
                        }
                        if (!snapshotComparaison(aSnapshot, weekDate) && snapshotComparaison(aSnapshot, selectedSnapshots[2])) { //week
                            selectedSnapshots[2] = aSnapshot
                        }
                        if (!snapshotComparaison(aSnapshot, monthDate) && snapshotComparaison(aSnapshot, selectedSnapshots[3])) { //month
                            selectedSnapshots[3] = aSnapshot
                        }
                    };

                    res.status(202).json({ snapshots: selectedSnapshots })
                }
                conn.end();
            });
        });
    }).connect({
        host: process.env.SSH_HOST,
        port: 22,
        username: process.env.SSH_USER,
        privateKey: fs.readFileSync(process.env.SSH_PRIVATEKEY)
    });
}
/**
 * Il prend un nombre et renvoie une chaîne de ce nombre avec des zéros non significatifs
 * @param time - le temps
 */
const timeFormat = (time) => {
    timeString = time.toString();
    zero = "00"
    return zero.substring(timeString.length) + timeString //Pour avoir le format 001
}
/**
 * Il compare deux chaînes, dont chacune est un nom de snapshot, et renvoie vrai si la première chaîne
 * est plus récente que la seconde
 * @param snapshotA - La snapshot que vous souhaitez comparer à l'autre.
 * @param snapshotB - La snapshot que vous souhaitez comparer
 * @returns true si la snapshot A est plus récent que la snapshot B, false sinon.
 * 
 */
const snapshotComparaison = (snapshotA, snapshotB) => {
    snapshotA = snapshotA.split("_")
    snapshotA = snapshotA[1].split("-")
    snapshotTimeA = [snapshotA[4].substring(0, 2), snapshotA[4].substring(2)]

    snapshotB = snapshotB.split("_")
    snapshotB = snapshotB[1].split("-")
    snapshotTimeB = [snapshotB[4].substring(0, 2), snapshotB[4].substring(2)]

    a = snapshotTimeA[1] * 1 + snapshotTimeA[0] * 60 + snapshotA[3] * 1440 + snapshotA[2] * 43800 + snapshotA[1] * 525600
    b = snapshotTimeB[1] * 1 + snapshotTimeB[0] * 60 + snapshotB[3] * 1440 + snapshotB[2] * 43800 + snapshotB[1] * 525600
    if (a >= b) { //Si A>B alors A plus jeune que B
        return true
    } else {
        return false
    }
}

const maxAge = 60 * 60 * 1000 //En miliseconde, signifie 1 heure
/**
 * Il prend un identifiant comme argument et renvoie un jeton Web JSON signé avec l'identifiant comme
 * charge utile et le délai d'expiration défini sur la variable maxAge
 * @param id - L'identifiant de l'utilisateur
 * @returns Un jeton qui expire dans le délai spécifié dans la variable maxAge.
 */
const createToken = (id) => {
    return jwt.sign({ id }, process.env.TOKEN_SECRET, {
        expiresIn: maxAge
    })
}

/** C'est une fonction qui permet de sélectionner une snapshot à partir d'une sélection de l'utilisateur.
 * @requires username - Va servir pour avoir le homeDirectory dans la snapshot
 * @requires snapshot - Va servir pour avoir le chemin de la snapshot selectionné
 * @returns Un cookie avec le chemin de la snapshot et le username
*/
module.exports.selectSnapshot = (req, res) => {
    const { snapshot, username, group, redirection } = req.body
    let pathSnapshot = null
    if (redirection === "homes" || redirection === "windows2") {
        pathSnapshot = process.env.PATH_SNAPSHOT + redirection + "/.zfs/snapshot/" //!!!!!    
    } else {
        let redirectionFormat= redirection.split("/")
        pathSnapshot = process.env.PATH_SNAPSHOT + redirectionFormat[0] + "/" + redirectionFormat[1] + "/.zfs/snapshot/"
    }
    const conn = new Client();
    conn.on('ready', () => {
        conn.sftp((err, sftp) => {
            if (err) throw err;
            sftp.readdir(pathSnapshot, (err, snapshots) => {
                if (err) throw err;
                snapshotsFormat = []
                snapshots.forEach(aSnapshot => { // Pour avoir les snapshots datées
                    if (aSnapshot["filename"].split("_").length == 2) snapshotsFormat.push(aSnapshot["filename"])
                });
                if (snapshotsFormat.indexOf(snapshot) !== -1) {
                    const token = createToken({ snapshot: snapshot, username: username, group: group, redirection: redirection })
                    res.cookie("snapshot", token, { httpOnly: true, maxAge: maxAge })
                    res.status(202).json({ snapshot: snapshot, username: username, group: group, redirection: redirection })
                } else {
                    res.status(404).send({ message: "Snapshot inconnue !" });
                }


                conn.end();
            });
        });
    }).connect({
        host: process.env.SSH_HOST,
        port: 22,
        username: process.env.SSH_USER,
        privateKey: fs.readFileSync(process.env.SSH_PRIVATEKEY)
    });
}

/** Il supprime le cookie snapshot.
*/
module.exports.removeSnapshot = async (req, res) => {
    res.cookie("snapshot", "", { maxAge: 1 })
    res.redirect('/home')
}

/** !!!Fonction non utilisable!!!
 * Fonction utilisée pour récupérer un fichier ou un dossier à partir d'une snapshot ZFS. 
 * @requires path - Chemin absolu d'une snapshot du ZFS 
 * @requires isDir - Si c'est un dossier, il faut ajouter un /
 * @returns Un message pour savoir si la copie a fonctionné ou non
*/
module.exports.recover = async (req, res) => {
    const { path, isDir } = req.body;
    const sshconfig = {
        host: process.env.SSH_HOST,
        port: 22,
        username: process.env.SSH_USER,
        privateKey: fs.readFileSync(process.env.SSH_PRIVATEKEY)
    }
    const ssh = new SSH2Promise(sshconfig);
    const sftp = ssh.sftp();

    const dirContent = await sftp.readdir(path)

    res.status(202).json({ dirContent })
}
/*
    const { path, isDir } = req.body;

    pathCopy = path
    pathPaste = path.split("/");
    if (isDir) {
        pathPaste.pop()
    }
    pathPaste.shift();
    pathPaste.splice(1, 3);
    pathPasteFormat = ""
    pathPaste.forEach(element => {
        pathPasteFormat = pathPasteFormat + "/" + element
    });
    if (isDir) {
        pathPasteFormat = pathPasteFormat + "/"
        pathCopy = path + "/"
    }

    await exec(`"./scripts/copie.sh" ${pathCopy} ${pathPasteFormat}`,
        (error, stdout, stderr) => {
            if (error != null) {
                console.log(error);
                res.status(200).send({ error: "Echec !" })
            } else {
                res.status(202).json({ message: "Réussite de la copie !" })
            }
        });
*/

/** Il télécharge le fichier à partir d'un chemin pour le navigateur de fichiers
 * Utilise une connexion SSH où les paramères sont des variables d'environnement
 * @requires fileP - Le chemin absolu du fichier ou dossier
 * @requires isDir - Booléen pour savoir si c'est un dossier 
 * @returns Permet de télécharger le fichier pour l'utilisateur
*/
module.exports.downloadFile = async (req, res) => {
    const { fileP, isDir } = req.body;
    const sshconfig = {
        host: process.env.SSH_HOST,
        port: 22,
        username: process.env.SSH_USER,
        privateKey: fs.readFileSync(process.env.SSH_PRIVATEKEY)
    }
    const ssh = new SSH2Promise(sshconfig);
    const sftp = ssh.sftp();

    if (isDir) {
        const zip = new AdmZip();
        const findFiles = async (path) => {
            const AllFiles = async (dir, pattern, recursive) => {
                if (typeof pattern === "boolean") {
                    recursive = pattern;
                    pattern = undefined;
                }
                let files = [];
                const dirContent = await sftp.readdir(dir)
                for (const file of dirContent) {
                    var path = pth.join(dir, file["filename"]);

                    const fileStat = await sftp.stat(path)
                    if (fileStat.isDirectory()) {
                        files = files.concat(await AllFiles(path, pattern, recursive))
                    }
                    if (!pattern || pattern.test(path)) {
                        files.push(path);
                    }
                }
                return files;
            }

            return await AllFiles(path, undefined, true)
        }
        const items = await findFiles(fileP);
        const fileToZip = async (items) => {
            if (items.length) {
                for (const filepath of items) {
                    var p = pth.relative(fileP, filepath).split("\\").join("/"); //windows fix
                    var stat = await sftp.stat(filepath)
                    if (stat.isFile()) { // fichier
                        var file = await sftp.readFile(filepath)
                        zip.addFile("" + p, file, "", stat);
                    } else { // dossier
                        zip.addFile("" + p + "/", Buffer.alloc(0), "", stat);
                    }
                };
            }
        }
        await fileToZip(items)
        var zipFileContents = zip.toBuffer();
        const fileName = fileP;
        const fileType = 'application/zip';
        res.writeHead(200, {
            'Content-Disposition': `attachment; filename="${fileName}"`,
            'Content-Type': fileType,
        })

        return res.end(zipFileContents.toString("base64"));
    } else {
        const filename = pth.basename(fileP)
        const mimetype = mime.getType(fileP)

        const fileStream = await sftp.createReadStream(fileP)
        res.writeHead(200, {
            'Content-disposition': `attachment; filename="${filename}"`,
            'Content-Type': mimetype,
        })
        await fileStream.pipe(res)
    }
}
/**
 * Il prend une liste de fichiers (inspiré par la commande : ls -R -A) et crée un objet JSON
 * @param files - un tableau de noms de fichiers, y compris des répertoires.
 * @param snapHomeDirectory - Le dossier racine
 * @returns Une promesse qui se résout en un objet JSON.
 */
const createJSON = (files, snapHomeDirectory) => {
    return new Promise((resolve) => {
        root = {}
        rootFolderId = snapHomeDirectory.split("/")
        rootFolderId = last(rootFolderId)
        root.rootFolderId = rootFolderId
        fileMap = {};
        root["fileMap"] = fileMap;
        for (const element of files) {
            if (element.charAt(0) == "/") { //Traitement du dossier
                identityParentId = element.slice(0, -1) //Enleve le :
                if (identityParentId == snapHomeDirectory) { //C'est le dossier root
                    identityParentName = identityParentId.split("/")
                    identityParentName = identityParentName.pop()
                    root.rootFolderId = identityParentId
                    root.fileMap[identityParentId] = { id: identityParentId, name: identityParentName, isDir: true, childrenIds: [], childrenCount: 0 }
                    emptyFolder = true
                } else { // C'est un autre dossier, donc juste ajout de propriété       
                    identityParentName = identityParentId.split("/")
                    identityParentName = identityParentName.pop()
                    root.fileMap[identityParentId].childrenIds = []
                    root.fileMap[identityParentId].isDir = true
                    root.fileMap[identityParentId].childrenCount = 0
                    emptyFolder = true
                }

            } else {  //Tous les enfants du dossier
                emptyFolder = false
                identityChildId = identityParentId + "/" + element
                root.fileMap[identityChildId] = { id: identityChildId, name: element, parentId: identityParentId, isDir: false, draggable: false }
                root.fileMap[identityParentId].childrenIds.push(identityChildId)
                root.fileMap[identityParentId].childrenCount++
                if (element.charAt(0) == ".") {
                    root.fileMap[identityChildId].isHidden = true
                }
            }
        };
        return resolve(root)
    })
}
/** C'est une fonction qui est utilisée pour obtenir l'arborescence des fichiers du répertoire personnel
 * de l'utilisateur
 * Utilise une connexion SSH où les paramères sont des variables d'environnement
 * @requires snapHomeDirectory - Chemin absolu d'un dossier.
 * @returns Un fichier JSON utilisé Chonky fileBrowser. 
*/
module.exports.snapshotHomeJSON = async (req, res) => {
    const { redirection, user, snapshot, group } = req.body;
    let isAdmin = false;
    if (group.includes("si")){
        isAdmin = true;
    }
    const sshconfig = {
        host: process.env.SSH_HOST,
        port: 22,
        username: process.env.SSH_USER,
        privateKey: fs.readFileSync(process.env.SSH_PRIVATEKEY)
    }
    const ssh = new SSH2Promise(sshconfig);
    let snapHomeDirectory = process.env.PATH_SNAPSHOT
    if (redirection == "windows2") {
        snapHomeDirectory = snapHomeDirectory + redirection + "/.zfs/snapshot/" + snapshot + "/redirections/" + user
    } else if (redirection == "homes") {
        snapHomeDirectory = snapHomeDirectory + redirection + "/.zfs/snapshot/" + snapshot + "/" + user
    } else { // Redirection compliquée
        let redirectionFormat= redirection.split("/")
        snapHomeDirectory = snapHomeDirectory + redirectionFormat[0] + "/" + redirectionFormat[1] + "/.zfs/snapshot/" + snapshot + "/" + redirectionFormat[2]
    }
    const sftp = ssh.sftp();
    const getAllFiles = async (dirPath, arrayOfFiles) => {
        try {
            const files = await sftp.readdir(dirPath)
            arrayOfFiles = arrayOfFiles || []
            arrayOfFiles.push(`${dirPath}:`)
            for (const file of files) {
                if ((file["filename"] !== ".cache" && isAdmin) || (!isAdmin && file["filename"].charAt(0) !== ".")) {
                    arrayOfFiles.push(`${file["filename"]}`)
                }
            }
            for (const file of files) {
                if ((file["longname"].charAt(0) === "d" && isAdmin && file["filename"] !== ".cache") || (file["longname"].charAt(0) === "d" && file["filename"].charAt(0) !== "." && !isAdmin)) {
                    arrayOfFiles = await getAllFiles(dirPath + "/" + file["filename"], arrayOfFiles)
                }
            }
            return arrayOfFiles
        } catch (error) {
            throw error + "|||" + dirPath
        }
    }
    try {
        const data = await getAllFiles(snapHomeDirectory);
        const root = await createJSON(data, snapHomeDirectory)
        res.status(202).json({ root })
    } catch (error) {
        const pathError = error.split("|||")[1]
        console.log("Erreur, redirection inconnue:", pathError);
        if (pathError.length === snapHomeDirectory.length) {
            res.status(404).send("A priori, vous n'avez pas de redirection")
        } else {
            const directoryError = pathError.slice(snapHomeDirectory.length, -1)
            res.status(404).send(`Le dossier '${directoryError}' pose problème`)
        }
    }

};

module.exports.getRedirection = async (req, res) => {
    const { memberOf } = req.body;
    fs.readFile("/usr/share/interfacewebzfsrecuperation/config/redirection.json", "utf-8", (err, redirectionList) => {
        if (err) {
            console.log(err);
            res.status(500).send("Erreur lors de la lecture du fichier de configuration");
        } else {
            const redirectionListJSON = JSON.parse(redirectionList)
            let redirectionListAvailable = {}
            //for loop in a objet to find the redirection in the list
            for (const aRedirection in redirectionListJSON) {
                if(memberOf.includes(aRedirection)){
                    redirectionListAvailable[aRedirection] = redirectionListJSON[aRedirection]
                }
            }
            res.status(202).json({redirectionListAvailable})
        }
    })
}