const UserModel = require('../models/user.model')
const jwt = require("jsonwebtoken")
const { signInErrors } = require('../utils/errors.utils');

const maxAge = 24 * 60 * 60 * 1000 //En miliseconde, signifie 1 jour

/**
 * Il prend un identifiant comme argument et renvoie un jeton Web JSON signé avec l'identifiant comme
 * charge utile et le délai d'expiration défini sur la variable maxAge
 * @param id - L'identifiant de l'utilisateur
 * @returns Un jeton qui expire dans le délai spécifié dans la variable maxAge.
 */
const createToken = (id) => {
    return jwt.sign({ id }, process.env.TOKEN_SECRET, {
        expiresIn: maxAge
    })
}


/** Cette fonction est utilisé quand l'utilisateur veut se connecter. Utilise l'annuaire LDAP de l'IUT -> Voir UserModel.login
 * @requires username - Identifiant de l'utilisateur
 * @requires password - Mot de passe de l'utilisateur
 * @returns Un cookie contenant des infos de l'utilisateur, sinon une erreur 
 */
module.exports.signIn = async (req, res) => {
    const { username, password } = req.body;
    try {
        const user = await UserModel.login(username, password); //Login avec LDAP
        if (user < 0) { throw error }
        const token = createToken(user)
        res.cookie("jwt", token, { httpOnly: true, maxAge: maxAge })
        console.log("Une connexion à",new Date());
        res.status(202).json({ user: user })
    } catch (error) {
        const errors = signInErrors(error)
        res.status(400).send({ errors });
    }
}

/** C'est une fonction qui est appelée lorsque l'utilisateur clique sur le bouton de déconnexion. Il
 * supprime le cookie et redirige l'utilisateur vers la page d'accueil.
 * @returns Retire le cookie appelé "jwt"
 * */
module.exports.logOut = (_req, res) => {
    res.cookie("jwt", "", { maxAge: 1 })
    res.redirect('/');
}