const express = require('express');
const path = require("path")
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser')
const userRoutes = require('./routes/user.routes');
const zfsRoutes = require('./routes/zfs.routes');
require('dotenv').config({ path: './config/.env' })
const { requireAuth } = require('./middlewares/auth.middleware')
const { requireSnap } = require('./middlewares/zfs.middleware')
const fs = require("fs")

//const httpsOptions = {
//    key: fs.readFileSync('selfsigned.key'),
//    cert: fs.readFileSync('selfsigned.crt')
//};

const cors = require('cors');

const app = express();

var helmet = require('helmet');
app.use(helmet());

app.use(bodyParser.json()); //Met la requête au bon format
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser());

/* Le code ci-dessus configure les options CORS pour le serveur. */
const corsOptions = {
    origin: process.env.CLIENT_URL,
    credentials: true,
    'allowedHeaders': ['sessionId', 'Content-Type'],
    'exposedHeaders': ['sessionId'],
    'methods': 'GET,POST',
    'preflightContinue': false
}
app.use(cors(corsOptions))
app.use(express.urlencoded({ extended: true, limit: "1kb" }));
app.use(express.json({ limit: "1kb" }));
//jwt

//middleware
app.get("/jwtid", requireAuth, (req, res) => {
    res.status(200).send(res.locals.user)
})
app.get("/snapshot", requireSnap, (req, res) => {
    res.status(200).send(res.locals.user)
})


//routes
app.use('/api/user', userRoutes)
app.use('/api/zfs', zfsRoutes)

//Si je suis en production (ne pas oublier la variable NODE_ENV)
if(process.env.NODE_ENV === "production"){
    //Set static folder
    app.use(express.static("frontend/build"))

    app.get("*", (req, res) => {
        res.sendFile(path.resolve(__dirname, 'frontend', 'build', 'index.html'))
    })
}

//server
//https.createServer(httpsOptions, app).listen(process.env.PORT);
app.listen(process.env.PORT, () => {
    console.log(`Ouverture du serveur sur le port ${process.env.PORT}`)
})


module.exports = app
